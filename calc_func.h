#pragma once

#include <stdexcept>
#include <string>

#include "token_stream.h"
#include "variables.h"

Token_stream ts;

double expression();

double primary() {
  Token t = ts.get();
  switch (t.kind) {
    case '(': {
      double d = expression();
      t = ts.get();
      if (t.kind != ')') throw std::invalid_argument("'(' expected");
      return d;
    }
    case '-':
      return -primary();
    case '+':
      return primary();
    case number:
      return t.value;
    case name:
      return get_value(t.name);
    default:
      throw std::invalid_argument("primary expected");
  }
}

double term() {
  double left = primary();
  while (true) {
    Token t = ts.get();
    switch (t.kind) {
      case '*':
        left *= primary();
        break;
      case '/': {
        double d = primary();
        if (d == 0) throw std::runtime_error("divide by zero");
        left /= d;
        break;
      }
      default:
        ts.unget(t);
        return left;
    }
  }
}

double expression() {
  double left = term();
  while (true) {
    Token t = ts.get();
    switch (t.kind) {
      case '+':
        left += term();
        break;
      case '-':
        left -= term();
        break;
      default:
        ts.unget(t);
        return left;
    }
  }
}

double declaration() {
  Token t = ts.get();
  if (t.kind != name)
    throw std::invalid_argument("name expected in declaration");
  std::string name = t.name;
  if (is_declared(name)) throw std::runtime_error("declared twice");
  Token t2 = ts.get();
  if (t2.kind != '=')
    throw std::invalid_argument("= missing in declaration of ");
  double d = expression();
  names.push_back(Variable(name, d));
  return d;
}

double statement() {
  Token t = ts.get();
  switch (t.kind) {
    case let:
      return declaration();
    default:
      ts.unget(t);
      return expression();
  }
}

void clean_up_mess() { ts.ignore(print); }

const std::string prompt = "> ";
const std::string result = "= ";

void calculate() {
  while (true) try {
      std::cout << prompt;
      Token t = ts.get();
      while (t.kind == print) t = ts.get();
      if (t.kind == quit) return;
      ts.unget(t);
      std::cout << result << statement() << std::endl;
    } catch (std::runtime_error& e) {
      std::cerr << e.what() << std::endl;
      clean_up_mess();
    }
}