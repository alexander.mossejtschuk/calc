#include <iostream>
#include <stdexcept>

#include "calc_func.h"

int main() {
  try {
    calculate();
    return 0;
  } catch (std::exception& e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    char c;
    while (std::cin >> c && c != ';')
      ;
    return 1;
  } catch (...) {
    std::cerr << "Ooops... unknown exception\n";
    char c;
    while (std::cin >> c && c != ';')
      ;
    return 2;
  }
}