#pragma once

#include <iostream>
#include <stdexcept>
#include <string>

const char let = 'L';
const char quit = 'Q';
const char print = ';';
const char number = '8';
const char name = 'a';

struct Token {
  char kind;
  double value;
  std::string name;
  Token(char ch) : kind(ch), value(0) {}
  Token(char ch, double val) : kind(ch), value(val) {}
  Token(char ch, std::string n) : kind(ch), name(n) {}
};

class Token_stream {
 public:
  Token_stream() : full(false), buffer(Token('0')) {}

  Token get() {
    if (full) {
      full = false;
      return buffer;
    }
    char ch;
    std::cin >> ch;
    switch (ch) {
      case '(':
      case ')':
      case '+':
      case '-':
      case '*':
      case '/':
      case '%':
      case ';':
      case '=':
        return Token(ch);
      case '.':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9': {
        std::cin.unget();
        double val;
        std::cin >> val;
        return Token(number, val);
      }
      default:
        if (isalpha(ch)) {
          std::cin.putback(ch);
          std::string s;
          std::cin >> s;

          if (s == "let") return Token(let);
          if (s == "quit") return Token(quit);
          return Token(name, s);
        }
        throw std::invalid_argument("Bad token");
    }
  }
  void unget(Token t) {
    buffer = t;
    full = true;
  }

  void ignore(char c) {
    if (full && c == buffer.kind) {
      full = false;
      return;
    }
    full = false;

    char ch;
    while (std::cin >> ch)
      if (ch == c) return;
  }

 private:
  bool full;
  Token buffer;
};