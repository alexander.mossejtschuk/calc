#pragma once

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

struct Variable {
  std::string name;
  double value;
  Variable(std::string n, double v) : name(n), value(v) {}
};

std::vector<Variable> names;

double get_value(std::string s) {
  for (int i = 0; i < names.size(); ++i)
    if (names[i].name == s) return names[i].value;
  throw std::runtime_error("get: undefined name ");
}

void set_value(std::string s, double d) {
  for (int i = 0; i < names.size(); ++i)
    if (names[i].name == s) {
      names[i].value = d;
      return;
    }
  throw std::runtime_error("set: undefined name ");
}

bool is_declared(std::string s) {
  for (int i = 0; i < names.size(); ++i)
    if (names[i].name == s) return true;
  return false;
}